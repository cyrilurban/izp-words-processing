/**
 * @file    proj1.c
 * @author  CYRIL URBAN
 * @date:   2015-11-08
 * @brief   Program pro jednoduche zpracovani slov v textu
 */

#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>

char vstup [101];
struct tm time_str;

int mystrlen(char vstup [101]) // detekce delky retezce
{
	int delka = 0;
	while (vstup[delka] != '\0')
	{
		delka++;
	}
	return delka;
}

bool isdate(char vstup [101]) // detekce date
{
	int delka = mystrlen(vstup);

	if (delka == 10
			&& isdigit(vstup[0])
			&& isdigit(vstup[1])
			&& isdigit(vstup[2])
			&& isdigit(vstup[3])
			&& vstup[4] == '-'
			&& isdigit(vstup[5])
			&& isdigit(vstup[6])
			&& vstup[7] == '-'
			&& isdigit(vstup[8])
			&& isdigit(vstup[9]))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isnumber(char vstup [101]) // detekce cisla
{
	int delka = mystrlen(vstup);

	for (int i = 0; i < delka; i++)
	{
		if (isdigit(vstup[i]))
		{
		}
		else
		{
			return false;
		}
	}
	return true;
}

bool ispalindrome(char vstup [101]) // detekce  palindromu
{
	int delka = mystrlen(vstup);

	if (delka == 1)
	{
		return true;
	}
	else
	{
		for (int i = (0); i < ((delka) / 2); i++)
		{
			if (vstup[i] == vstup[delka - 1 - i])
			{

			}
			else
			{
				return false;
			}
		}
		return true;
	}
}

bool isprime(char vstup [101]) // detekce  prvocisla
{
	int cislo = atoi(vstup);

	if (cislo > 1)
	{
		for (int i = 2; i < cislo; i++)
		{
			if (cislo % i == 0)
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}

}

int main(int argc, __attribute__((unused))char *argv[])
{

	if (argc > 1) // Pokud je program spusten s libovolnymi neprazdn�mi argumenty, vypise svuj kr�tk� popis a uspe�ne skonci
	{
		printf("IZP: projekt 1\nprogram pro jednoduche zpracovani slov v textu\nautor: Cyril Urban");
		return 0;
	}

	while (scanf("%100s", vstup) != EOF) // ctu znaky, dokud neni konec retezce
	{
		bool word = false, number = false, date = false; // deklarace boolu, vlozeno zde z duvodu false na zacatku kazdeho cyklu

		if (isdate(vstup)) // volani funkce isdate
		{
			date = true;
		}
		else if (isnumber(vstup)) // volani funkce isnumber
		{
			number = true;
		}
		else
		{
			word = true;
		}

		if (date) // rozpoznavani dnu v tydny z data
		{
			char daybuf[101];
			int year = ((vstup[0] - '0') * 1000) + ((vstup[1] - '0') * 100) + ((vstup[2] - '0') * 10) + (vstup[3] - '0'); // prevod charu na integer
			int month = ((vstup[5] - '0') * 10) + (vstup[6] - '0');
			int day = ((vstup[8] - '0') * 10) + (vstup[9] - '0');

			time_str.tm_year = year - 1900;
			time_str.tm_mon = month - 1;
			time_str.tm_mday = day;

			if ((mktime(&time_str) == -1) || (month > 12) || (day > 31)) // volani funkce mktime, primitivni rozpoznani spravnosti data
			{
				printf("word: %s\n", vstup);
			}
			else
			{
				(void)strftime(daybuf, sizeof(daybuf), "%a", &time_str);
				printf("date: %s %s\n", daybuf, vstup);
			}

		}

		else if (word) // rozpoznavani palindromu
		{
			if (ispalindrome(vstup) == false) // volani funkce ispalindrome
			{
				printf("word: %s\n", vstup);
			}
			else
			{
				printf("word: %s (palindrome)\n", vstup);
			}
		}

		else if (number)
		{
			int cislo = atoi(vstup); // reseni useknuti predni 0 (napr: 01 na 1)

			if ((mystrlen(vstup) == 10 && vstup[0] == '2' && (vstup[9]) > 7) // kontrola zda neni cislo vetsi nez INT MAX (2147483647)
					|| (mystrlen(vstup) > 10))
			{
				printf("number: invalid\n");
			}
			else  // rozpoznavani prvocisla
			{
				if (isprime(vstup) == false) // volani funkce isprime
				{
					printf("number: %d\n", cislo);
				}
				else
				{
					printf("number: %d (prime)\n", cislo);
				}
			}
		}
	}
	return 0;
}

